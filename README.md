#Timekiller Ultimate
An Android app written with Kotlin that helps people kill time during boring moments. The main features
include random comic strips, random jokes, random advice, and a trivia quiz. Also has a favourite feature
to bookmark certain entries.


